# Miscellaneous (one time)
kubectl create namespace  snowflake-poc;
kubectl port-forward $POD_NAME 8080:80

# Install
helm install --name snowflake-connector --namespace snowflake-poc .

# Upgrade
helm upgrade snowflake-connector .  --namespace snowflake-poc
 
# Scale
kubectl scale deployment.apps/snowflake-connector --replicas=3 -n snowflake-poc

